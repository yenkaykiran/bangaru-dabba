package yuown.bangarudabba.popup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;

import java.util.List;

import yuown.bangarudabba.BangaruDabbaMainActivity;
import yuown.bangarudabba.R;
import yuown.bangarudabba.dao.DealerDao;
import yuown.bangarudabba.dao.IncomingDao;
import yuown.bangarudabba.dao.OutgoingDao;
import yuown.bangarudabba.databinding.DealerPopupEditBinding;
import yuown.bangarudabba.databinding.FragmentSummaryBinding;
import yuown.bangarudabba.lib.Helper;
import yuown.bangarudabba.model.Dealer;
import yuown.bangarudabba.model.Incoming;
import yuown.bangarudabba.model.Outgoing;
import yuown.bangarudabba.model.SummaryModel;

/**
 * Created by Kiran.Nk on 9/25/2017.
 */

public class PopupHelper {

    private static DealerDao dealerDao = DealerDao.getInstance();
    private static IncomingDao incomingDao = IncomingDao.getInstance();
    private static OutgoingDao outgoingDao = OutgoingDao.getInstance();

    public static void showDealerEditPopup(final BangaruDabbaMainActivity activity, final Dealer dealer) {
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DealerPopupEditBinding dealerPopupEditBinding = DataBindingUtil.inflate(layoutInflater, R.layout.dealer_popup_edit, null, false);
        View dealerEditView = dealerPopupEditBinding.getRoot();

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
        alertBuilder.setView(dealerEditView);

        dealerPopupEditBinding.setDealer(dealer);
        // set dialog message
        alertBuilder
                .setCancelable(false)
                .setTitle("Dealer Name")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                // edit text
                                if (!BangaruDabbaMainActivity.getInstance().getDealersTab().isEmpty(dealer)) {
                                    dealerDao.save(dealer);
                                }
                                activity.getDealersTab().reloadEntries();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog dealerEditDialog = alertBuilder.create();
        // show it
        dealerEditDialog.show();
    }

    public static void showSummaryPopup(final BangaruDabbaMainActivity activity) {
        SummaryModel model = getSummaryModel();

        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FragmentSummaryBinding fragmentSummaryBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_summary, null, false);
        View summaryView = fragmentSummaryBinding.getRoot();
        fragmentSummaryBinding.setSummary(model);

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
        alertBuilder.setView(summaryView);

        // set dialog message
        alertBuilder
                .setCancelable(true)
                .setTitle("Summary")
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("Clear All Data",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                incomingDao.clearAll();
                                outgoingDao.clearAll();
                            }
                        });

        // create alert dialog
        AlertDialog summaryDialog = alertBuilder.create();
        // show it
        summaryDialog.show();
    }

    public static void showImportExportPopup(final BangaruDabbaMainActivity activity) {
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View importExportView = layoutInflater.inflate(android.R.layout.simple_dropdown_item_1line, null);

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
        alertBuilder.setView(importExportView);

        // set dialog message
        alertBuilder
                .setCancelable(true)
                .setTitle("Import/ Export")
                .setNeutralButton("Import",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Helper.importToRealmDb(activity);
                            }
                        })
                .setPositiveButton("Export",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Helper.exportRealmDb(activity);
                            }
                        });

        // create alert dialog
        AlertDialog importExportDialog = alertBuilder.create();
        // show it
        importExportDialog.show();
    }

    @NonNull
    private static SummaryModel getSummaryModel() {
        SummaryModel model = new SummaryModel();
        List<Incoming> in = incomingDao.getAll();
        List<Outgoing> out = outgoingDao.getAll();
        double inTotal = 0.0;
        for (Incoming i : in) {
            inTotal += Helper.stringToDouble(i.getNetPurity());
        }

        double outTotal = 0.0;
        for (Outgoing o : out) {
            outTotal += Helper.stringToDouble(o.getNetPurity());
        }

        model.setIncoming(Helper.toThreeDecimalPlaces(inTotal));
        model.setOutgoing(Helper.toThreeDecimalPlaces(outTotal));
        return model;
    }
}
