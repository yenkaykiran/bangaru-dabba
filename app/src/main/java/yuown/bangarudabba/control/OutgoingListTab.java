package yuown.bangarudabba.control;

import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.view.View;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

import yuown.bangarudabba.BangaruDabbaMainActivity;
import yuown.bangarudabba.R;
import yuown.bangarudabba.adapter.OutgoingAdapter;
import yuown.bangarudabba.dao.BangaruDabbaDao;
import yuown.bangarudabba.dao.IncomingDao;
import yuown.bangarudabba.dao.OutgoingDao;
import yuown.bangarudabba.databinding.OutgoingListBinding;
import yuown.bangarudabba.lib.Helper;
import yuown.bangarudabba.model.Dealer;
import yuown.bangarudabba.model.Outgoing;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class OutgoingListTab extends BangaruDabbaTab<Outgoing, OutgoingDao, OutgoingListBinding, OutgoingAdapter> {

    private static Dealer dealer;

    private IncomingDao incomingDao = IncomingDao.getInstance();

    public static Dealer getDealer() {
        return dealer;
    }

    public static void setDealer(Dealer dealer) {
        OutgoingListTab.dealer = dealer;
    }

    @Override
    protected OutgoingAdapter getAdapter() {
        OutgoingAdapter adapter = new OutgoingAdapter(getContext(), localList, this);
        return adapter;
    }

    @Override
    protected List<Outgoing> primaryQuery() {
        return getDao().getAllByDateAndDealer(getData().get(BangaruDabbaDao.DATE).toString(), OutgoingListTab.dealer.getId());
    }

    @Override
    protected boolean isEmpty(Outgoing local) {
        return StringUtils.isBlank(local.getPurity()) && StringUtils.isBlank(local.getAmount());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.outgoing_list;
    }

    @Override
    protected int getListId() {
        return R.id.outgoing_list;
    }

    @Override
    protected OutgoingDao getDao() {
        return OutgoingDao.getInstance();
    }

    @Override
    protected Outgoing newEntity() {
        Outgoing outgoing = new Outgoing();
        try {
            outgoing.setDate(getData().get(BangaruDabbaDao.DATE).toString());
            outgoing.setOfDate(sdf.parse(outgoing.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        outgoing.setDealerId(dealer.getId());
        return outgoing;
    }

    @Override
    public void optionalCreateView(OutgoingListBinding binding) {
        super.optionalCreateView(binding);
        dateControl = binding.dateControl;
        binding.dateControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),
                        dateSelector,
                        CALENDAR.get(Calendar.YEAR),
                        CALENDAR.get(Calendar.MONTH),
                        CALENDAR.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        binding.saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAllEntries();
                updateTotals();
            }
        });
        binding.setDealer(dealer);
        updateTotals();
    }

    public void optionalChangeView() {
        if (null != binding) {
            updateTotals();
        }

        binding.leftDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CALENDAR.set(Calendar.DATE, CALENDAR.get(Calendar.DATE) - 1);
                updateViews();
            }
        });
        binding.rightDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CALENDAR.set(Calendar.DATE, CALENDAR.get(Calendar.DATE) + 1);
                updateViews();
            }
        });
    }

    private void updateTotals() {
        double totalIncoming = incomingDao.getTotalNetpurityOfDealerUntilDate(dealer.getId(), getData().get(BangaruDabbaDao.DATE).toString());
        double totalOutgoing = getDao().getTotalNetpurityOfDealerUntilDate(dealer.getId(), getData().get(BangaruDabbaDao.DATE).toString());
        double difference = totalIncoming - totalOutgoing + getCurrentTotal();
        binding.incomingBalance.setText("Outgoing Total Till Now: " + Helper.toThreeDecimalPlaces(difference));
        binding.calculatedIncomingBalance.setText("Current Total: " + Helper.toThreeDecimalPlaces(difference - getCurrentTotal()));
    }

    private double getCurrentTotal() {
        double total = 0.0;
        for (Outgoing each : localList) {
            total += Helper.stringToDouble(each.getNetPurity());
        }
        return total;
    }
}
