package yuown.bangarudabba.control;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;

import yuown.bangarudabba.R;
import yuown.bangarudabba.adapter.DealerAdapter;
import yuown.bangarudabba.dao.DealerDao;
import yuown.bangarudabba.databinding.DealersListBinding;
import yuown.bangarudabba.model.Dealer;


/**
 * Created by k3n on 19/09/17.
 */

public class DealerListTab extends BangaruDabbaTab<Dealer, DealerDao, DealersListBinding, DealerAdapter> {

    @Override
    protected DealerAdapter getAdapter() {
        DealerAdapter adapter = new DealerAdapter(getContext(), localList);
        return adapter;
    }

    @Override
    protected List<Dealer> primaryQuery() {
        return getDao().getAll();
    }

    @Override
    public boolean isEmpty(Dealer local) {
        return StringUtils.isBlank(local.getName()); // && StringUtils.isBlank(local.getEmail()) && StringUtils.isBlank(local.getMobile());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dealers_list;
    }

    @Override
    protected int getListId() {
        return R.id.dealer_list;
    }

    @Override
    protected DealerDao getDao() {
        return DealerDao.getInstance();
    }

    @Override
    protected Dealer newEntity() {
        Dealer dealer = new Dealer();
        dealer.setCreated(new Date());
        return dealer;
    }
}
