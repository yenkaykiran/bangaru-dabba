package yuown.bangarudabba.control;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import yuown.bangarudabba.adapter.BangaruDabbaAdapter;
import yuown.bangarudabba.dao.BangaruDabbaDao;
import yuown.bangarudabba.lib.Helper;
import yuown.bangarudabba.model.BangaruDabbaEntity;
import yuown.bangarudabba.model.DateModel;

/**
 * Created by k3n on 21/09/17.
 */

public abstract class BangaruDabbaTab<Type extends BangaruDabbaEntity, Dao extends BangaruDabbaDao<Type>, Binding extends ViewDataBinding, Adapter extends BangaruDabbaAdapter> extends Fragment {

    public static final Calendar CALENDAR = Calendar.getInstance();
    public static final String DD_MMM_YYYY = "dd-MMM-yyyy";
    public static final SimpleDateFormat sdf = new SimpleDateFormat(DD_MMM_YYYY, Locale.US);
    private static Map<String, Object> data = new HashMap<String, Object>();
    protected EditText dateControl;
    protected List<Type> localList = new ArrayList<Type>();
    protected Binding binding;
    private DateModel model = new DateModel();
    private Adapter arrayAdapter = null;
    DatePickerDialog.OnDateSetListener dateSelector = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            CALENDAR.set(Calendar.YEAR, year);
            CALENDAR.set(Calendar.MONTH, monthOfYear);
            CALENDAR.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateViews();
        }

    };

    public static Map<String, Object> getData() {
        return data;
    }

    protected abstract List<Type> primaryQuery();

    protected abstract boolean isEmpty(Type local);

    protected abstract Adapter getAdapter();

    protected abstract int getLayoutId();

    protected abstract int getListId();

    protected abstract Dao getDao();

    protected abstract Type newEntity();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);

        View currentView = binding.getRoot();
        ListView listView = (ListView) currentView.findViewById(getListId());
        arrayAdapter = getAdapter();
        listView.setAdapter(arrayAdapter);

        optionalCreateView(binding);
        updateViews();
        return currentView;
    }

    public void optionalCreateView(Binding binding) {
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public final void saveAllEntries() {
        Helper.hideKeyboard(this.getActivity());
        for (Type local : localList) {
            if (!isEmpty(local)) {
                getDao().save(local);
            }
        }
        reloadEntries();
    }

    public final void reloadEntries() {
        localList.clear();
        List<Type> dbList = primaryQuery();
        localList.addAll(dbList);
        if (null != arrayAdapter) {
            arrayAdapter.notifyDataSetChanged();
        }
    }

    public void addNewEntry() {
        Type entry = newEntity();
        localList.add(entry);
        arrayAdapter.notifyDataSetChanged();
        Helper.hideKeyboard(this.getActivity());
    }

    public void updateViews() {
        model.setDate(sdf.format(CALENDAR.getTime()));
        model.setOfDate(CALENDAR.getTime());
        if (null != dateControl) {
            dateControl.setText(model.getDate());
        }
        dateChanged();
        optionalChangeView();
    }

    public void optionalChangeView() {
    }

    private void dateChanged() {
        getData().put(BangaruDabbaDao.DATE, model.getDate());
        getData().put(BangaruDabbaDao.OF_DATE, model.getOfDate());
        reloadEntries();
    }
}
