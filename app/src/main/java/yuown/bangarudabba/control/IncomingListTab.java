package yuown.bangarudabba.control;

import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.view.View;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

import yuown.bangarudabba.BangaruDabbaMainActivity;
import yuown.bangarudabba.R;
import yuown.bangarudabba.adapter.IncomingAdapter;
import yuown.bangarudabba.dao.BangaruDabbaDao;
import yuown.bangarudabba.dao.IncomingDao;
import yuown.bangarudabba.databinding.IncomingListBinding;
import yuown.bangarudabba.lib.Helper;
import yuown.bangarudabba.model.Dealer;
import yuown.bangarudabba.model.Incoming;
import yuown.bangarudabba.model.Outgoing;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class IncomingListTab extends BangaruDabbaTab<Incoming, IncomingDao, IncomingListBinding, IncomingAdapter> {

    private static Dealer dealer;

    public static Dealer getDealer() {
        return dealer;
    }

    public static void setDealer(Dealer dealer) {
        IncomingListTab.dealer = dealer;
    }

    @Override
    protected IncomingAdapter getAdapter() {
        IncomingAdapter adapter = new IncomingAdapter(getContext(), localList);
        return adapter;
    }

    @Override
    protected List<Incoming> primaryQuery() {
        return getDao().getAllByDateAndDealer(getData().get(BangaruDabbaDao.DATE).toString(), IncomingListTab.dealer.getId());
    }

    @Override
    public boolean isEmpty(Incoming local) {
        return StringUtils.isBlank(local.getName()) && StringUtils.isBlank(local.getPurity()) && StringUtils.isBlank(local.getWeight());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.incoming_list;
    }

    @Override
    protected int getListId() {
        return R.id.incoming_list;
    }

    @Override
    public IncomingDao getDao() {
        return IncomingDao.getInstance();
    }

    @Override
    public Incoming newEntity() {
        Incoming incoming = new Incoming();
        try {
            incoming.setDate(getData().get(BangaruDabbaDao.DATE).toString());
            incoming.setOfDate(sdf.parse(incoming.getDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        incoming.setDealerId(dealer.getId());
        return incoming;
    }

    @Override
    public void optionalCreateView(IncomingListBinding binding) {
        super.optionalCreateView(binding);
        dateControl = binding.dateControl;
        binding.dateControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),
                        dateSelector,
                        CALENDAR.get(Calendar.YEAR),
                        CALENDAR.get(Calendar.MONTH),
                        CALENDAR.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        binding.saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAllEntries();
                updateTotals();
            }
        });
        binding.setDealer(dealer);

        binding.leftDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CALENDAR.set(Calendar.DATE, CALENDAR.get(Calendar.DATE) - 1);
                updateViews();
            }
        });
        binding.rightDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CALENDAR.set(Calendar.DATE, CALENDAR.get(Calendar.DATE) + 1);
                updateViews();
            }
        });
        updateTotals();
    }

    @Override
    public void optionalChangeView() {
        updateTotals();
    }

    private double getCurrentTotal() {
        double total = 0.0;
        for (Incoming each : localList) {
            total += Helper.stringToDouble(each.getNetPurity());
        }
        return total;
    }

    private void updateTotals() {
        double totalIncoming = getDao().getTotalNetpurityOfDealerUntilDate(dealer.getId(), getData().get(BangaruDabbaDao.DATE).toString());
        binding.calculatedOutgoingBalance.setText("Current Total: " + Helper.toThreeDecimalPlaces(getCurrentTotal()));
    }
}
