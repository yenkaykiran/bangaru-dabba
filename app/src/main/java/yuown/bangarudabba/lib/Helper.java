package yuown.bangarudabba.lib;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import yuown.bangarudabba.BangaruDabbaMainActivity;
import yuown.bangarudabba.model.Dealer;

/**
 * Created by k3n on 10/09/17.
 */

public class Helper {

    private static final DecimalFormat format = new DecimalFormat("##.###");

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static File EXPORT_REALM_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    public static String REALM_FILE_NAME = "bangaru-dabba.realm";
    public static String REALM_NAME = "bangaru-dabba";
//    public static String REALM = ".realm";
    public static final String DD_MMM_YYYY_HH_MM_SS = "dd-MMM-yyyy_hh_mm_ss";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DD_MMM_YYYY_HH_MM_SS, Locale.US);

    public static String toThreeDecimalPlaces(String value) {
        if (StringUtils.isEmpty(value)) {
            value = "0.0";
        }
        return format.format(value);
    }

    public static String toThreeDecimalPlaces(double value) {
        return format.format(Double.valueOf(value));
    }

    public static double stringToDouble(String value) {
        if (StringUtils.isEmpty(value)) {
            value = "0.0";
        }
        return Double.parseDouble(value);
    }

    public static double stringToDouble(double value) {
        return value;
    }

    public static void hideKeyboard(Activity activity) {
        try {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }

    public static int getDealerIdFromPosition(List<Dealer> dealers, String id) {
        int position = 0;
        for (int i = 0; i < dealers.size(); i++) {
            Dealer dealer = dealers.get(i);
            if (StringUtils.equals(dealer.getId(), id)) {
                position = i;
                break;
            }
        }
        return position;
    }

    public static void exportRealmDb(BangaruDabbaMainActivity activity) {
        final Realm realm = Realm.getDefaultInstance();
        File exportRealmFile;
        try {
            checkStoragePermissions(activity);
            EXPORT_REALM_PATH.mkdirs();
            // create a backup file
            exportRealmFile = new File(EXPORT_REALM_PATH, REALM_FILE_NAME);

            // if backup file already exists, delete it
            exportRealmFile.delete();

            // copy current realm to backup file
            realm.writeCopyTo(exportRealmFile);
            Toast.makeText(activity, "Data Successfully exported to: " + exportRealmFile, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(REALM_NAME, e.getMessage());
            Toast.makeText(activity, "Failed to export: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public static void importToRealmDb(BangaruDabbaMainActivity activity) {
        try {
            checkStoragePermissions(activity);
            //Restore
            String restoreFilePath = EXPORT_REALM_PATH + "/" + REALM_FILE_NAME;

            Log.d(REALM_NAME, "Old File Path = " + restoreFilePath);

            copyBundledRealmFile(activity, restoreFilePath, REALM_FILE_NAME);
            Toast.makeText(activity, "Data Successfully imported", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(REALM_NAME, e.getMessage());
            Toast.makeText(activity, "Failed to import: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static String copyBundledRealmFile(BangaruDabbaMainActivity activity, String oldFilePath, String outFileName) throws IOException {
        try {
            File file = new File(activity.getApplicationContext().getFilesDir(), outFileName);

            FileOutputStream outputStream = new FileOutputStream(file);

            FileInputStream inputStream = new FileInputStream(new File(oldFilePath));

            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, bytesRead);
            }
            outputStream.close();
            return file.getAbsolutePath();
        } catch (Exception e) {
            Log.e(REALM_NAME, e.getMessage());
            throw e;
        }
    }


    private static void checkStoragePermissions(BangaruDabbaMainActivity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if(permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
