package yuown.bangarudabba.model;

import io.realm.RealmModel;

/**
 * Created by kiran.nk on 9/22/2017.
 */

public interface BangaruDabbaEntity extends RealmModel {

    public String getId();

    public void setId(String id);

    public String getNetPurity();

    public void setNetPurity(String netPurity);

}
