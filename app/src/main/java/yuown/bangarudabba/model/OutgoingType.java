package yuown.bangarudabba.model;

/**
 * Created by k3n on 18/09/17.
 */

public enum OutgoingType {

    METAL, CASH
}
