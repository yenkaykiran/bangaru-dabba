package yuown.bangarudabba.model;

import java.util.Date;

/**
 * Created by k3n on 15/09/17.
 */

public class DateModel {

    private String date;
    private Date ofDate;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Date getOfDate() {
        return ofDate;
    }

    public void setOfDate(Date ofDate) {
        this.ofDate = ofDate;
    }
}
