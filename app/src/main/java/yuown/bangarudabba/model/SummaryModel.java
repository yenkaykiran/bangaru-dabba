package yuown.bangarudabba.model;

/**
 * Created by k3n on 18/09/17.
 */

public class SummaryModel {

    private String incoming;
    private String outgoing;

    public String getIncoming() {
        return incoming;
    }

    public void setIncoming(String incoming) {
        this.incoming = incoming;
    }

    public String getOutgoing() {
        return outgoing;
    }

    public void setOutgoing(String outgoing) {
        this.outgoing = outgoing;
    }
}
