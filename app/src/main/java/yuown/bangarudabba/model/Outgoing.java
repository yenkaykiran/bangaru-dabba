package yuown.bangarudabba.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;

import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by k3n on 18/09/17.
 */
@RealmClass
public class Outgoing implements BangaruDabbaEntity {

    @PrimaryKey
    private String id;
    private String outgoingType;
    private String amount;
    private String purity;
    private String netPurity;
    private String date;
    private Date ofDate;
    private String dealerId;

    public String getOutgoingType() {
        if (StringUtils.isEmpty(outgoingType)) {
            outgoingType = OutgoingType.METAL.toString();
        }
        return outgoingType;
    }

    public void setOutgoingType(String outgoingType) {
        this.outgoingType = outgoingType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPurity() {
        return purity;
    }

    public void setPurity(String purity) {
        this.purity = purity;
    }

    public String getNetPurity() {
        return netPurity;
    }

    public void setNetPurity(String netPurity) {
        this.netPurity = netPurity;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public Date getOfDate() {
        return ofDate;
    }

    public void setOfDate(Date ofDate) {
        this.ofDate = ofDate;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, false);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, false);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
