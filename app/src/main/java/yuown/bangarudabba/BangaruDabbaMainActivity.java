package yuown.bangarudabba;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import yuown.bangarudabba.control.BangaruDabbaTab;
import yuown.bangarudabba.control.DealerListTab;
import yuown.bangarudabba.model.Dealer;
import yuown.bangarudabba.model.Frag;
import yuown.bangarudabba.popup.PopupHelper;

import static yuown.bangarudabba.lib.Helper.REALM_FILE_NAME;

public class BangaruDabbaMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static Realm realm = null;

    public static BangaruDabbaMainActivity instance;

    private DealerListTab dealersTab;

    private Frag currentFragment = Frag.DEALER;

    private BangaruDabbaTab tab;

    public static Realm getRealm() {
        return realm;
    }

    public static BangaruDabbaMainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bangaru_dabba_main);
        // AppBarBangaruDabbaMainBinding appBarBangaruDabbaMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_bangaru_dabba_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (currentFragment) {
                    case DEALER:
                        PopupHelper.showDealerEditPopup(BangaruDabbaMainActivity.instance, new Dealer());
                        break;
                    case INCOMING:
                    case OUTGOING:
                        tab.addNewEntry();
                        break;
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        dealersTab = new DealerListTab();
        initializeDatabase();
        instance = this;
        replaceFragment(dealersTab, Frag.DEALER, false);
    }

    public void replaceFragment(final BangaruDabbaTab tab, Frag currentFragment, boolean addToStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction().replace(R.id.main_content, tab);
        this.currentFragment = currentFragment;
        this.tab = tab;
        if (addToStack) {
            ft.addToBackStack(tab.getClass().toString());
        }
        if (tab instanceof DealerListTab) {
            ft.commitNow();
        } else {
            ft.commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        this.currentFragment = Frag.DEALER;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_summary) {
            PopupHelper.showSummaryPopup(BangaruDabbaMainActivity.instance);
        } else if (id == R.id.nav_import_export) {
            PopupHelper.showImportExportPopup(BangaruDabbaMainActivity.instance);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initializeDatabase() {
        Realm.init(getApplicationContext());

        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .name(REALM_FILE_NAME)
                .schemaVersion(1)
                .build();

        Realm.setDefaultConfiguration(config);
        realm = Realm.getInstance(config);
    }

    public DealerListTab getDealersTab() {
        return dealersTab;
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }
}
