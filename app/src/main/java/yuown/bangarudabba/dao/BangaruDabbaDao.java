package yuown.bangarudabba.dao;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import io.realm.Realm;
import yuown.bangarudabba.BangaruDabbaMainActivity;
import yuown.bangarudabba.lib.Helper;
import yuown.bangarudabba.model.BangaruDabbaEntity;

import static yuown.bangarudabba.control.BangaruDabbaTab.sdf;

/**
 * Created by kiran.nk on 9/22/2017.
 */

public abstract class BangaruDabbaDao<Type extends BangaruDabbaEntity> {

    public static final String ID = "id";
    public static final String DATE = "date";
    public static final String OF_DATE = "ofdate";

    protected static Realm realm = BangaruDabbaMainActivity.getRealm();

    private Class<Type> type;

    private BangaruDabbaDao() {
    }

    protected BangaruDabbaDao(Class<Type> type) {
        this.type = type;
    }

    public List<Type> getAll() {
        List<Type> allFromDb = new ArrayList<Type>();
        realm.beginTransaction();
        List<Type> all = realm.where(type).findAll();
        realm.commitTransaction();
        for (Type dbEntry : all) {
            allFromDb.add(transformEntity(dbEntry));
        }
        return allFromDb;
    }

    public Type getOne(String id) {
        realm.beginTransaction();
        Type one = realm.where(type).equalTo(ID, id).findFirst();
        realm.commitTransaction();
        return transformEntity(one);
    }

    public List<Type> getAllByDateAndDealer(String date, String dealerId) {
        List<Type> allFromDb = new ArrayList<Type>();
        realm.beginTransaction();
        List<Type> all = realm.where(type).equalTo(DATE, date).equalTo("dealerId", dealerId).findAll();
        realm.commitTransaction();
        for (Type dbEntry : all) {
            allFromDb.add(transformEntity(dbEntry));
        }
        return allFromDb;
    }

    public Type save(Type entity) {
        realm.beginTransaction();
        if (StringUtils.isBlank(entity.getId())) {
            entity.setId(UUID.randomUUID().toString());
        }
        entity = transformEntity(realm.copyToRealmOrUpdate(transformEntity(entity)));
        realm.commitTransaction();
        return entity;
    }

    public List<Type> save(List<Type> entities) {
        realm.beginTransaction();
        List<Type> saved = new ArrayList<Type>();
        entities = realm.copyToRealmOrUpdate(entities);
        for (Type s : saved) {
            saved.add(transformEntity(s));
        }
        realm.commitTransaction();
        return saved;
    }

    public void remove(Type entity) {
        realm.beginTransaction();
        realm.where(type).equalTo(ID, entity.getId()).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void clearAll() {
        realm.beginTransaction();
        realm.where(type).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public Double getTotalNetpurityOfDealerUntilDate(String dealerId, String date) {
        Date endDate = null, startDate = null;
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(sdf.parse(date));

            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 23, 59, 59);

            endDate = calendar.getTime();

            calendar.set(1900, calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 1);
            startDate = calendar.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Double total = 0.0;
        realm.beginTransaction();
        List<Type> all = realm.where(type)
                .equalTo("dealerId", dealerId)
                .between("ofDate", startDate, endDate)
                .findAll();
        for (Type each : all) {
            total += Helper.stringToDouble(each.getNetPurity());
        }
        realm.commitTransaction();
        return total;
    }

    public abstract Type transformEntity(Type from);

    public abstract Type newEntity(String id);

    public abstract Set<String> getItemNamesStartingWith(String nameLike);
}
