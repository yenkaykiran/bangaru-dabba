package yuown.bangarudabba.dao;

import java.util.HashSet;
import java.util.Set;

import yuown.bangarudabba.model.Dealer;

/**
 * Created by k3n on 18/09/17.
 */

public class DealerDao extends BangaruDabbaDao<Dealer> {

    private static final DealerDao INSTANCE = new DealerDao();

    private DealerDao() {
        super(Dealer.class);
    }

    public static DealerDao getInstance() {
        return INSTANCE;
    }

    @Override
    public Dealer newEntity(String id) {
        Dealer dealer = new Dealer();
        dealer.setId(id);
        return dealer;
    }

    @Override
    public Set<String> getItemNamesStartingWith(String nameLike) {
        return new HashSet<String>();
    }

    @Override
    public Dealer transformEntity(Dealer from) {
        Dealer to = new Dealer();
        to.setId(from.getId());
        to.setName(from.getName());
        to.setCreated(from.getCreated());
        return to;
    }
}