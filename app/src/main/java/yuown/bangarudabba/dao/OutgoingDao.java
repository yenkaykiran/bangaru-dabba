package yuown.bangarudabba.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import yuown.bangarudabba.control.BangaruDabbaTab;
import yuown.bangarudabba.control.OutgoingListTab;
import yuown.bangarudabba.model.Outgoing;

/**
 * Created by k3n on 18/09/17.
 */

public class OutgoingDao extends BangaruDabbaDao<Outgoing> {

    private static final OutgoingDao INSTANCE = new OutgoingDao();

    private OutgoingDao() {
        super(Outgoing.class);
    }

    public static OutgoingDao getInstance() {
        return INSTANCE;
    }

    @Override
    public Outgoing transformEntity(Outgoing from) {
        Outgoing to = new Outgoing();
        to.setId(from.getId());
        to.setOutgoingType(from.getOutgoingType());
        to.setAmount(from.getAmount());
        to.setPurity(from.getPurity());
        to.setNetPurity(from.getNetPurity());
        to.setDate(from.getDate());
        to.setOfDate(from.getOfDate());
        to.setDealerId(from.getDealerId());
        return to;
    }

    @Override
    public Outgoing newEntity(String id) {
        Outgoing outgoing = new Outgoing();
        outgoing.setDate(BangaruDabbaTab.getData().get(BangaruDabbaDao.DATE).toString());
        outgoing.setOfDate((Date) BangaruDabbaTab.getData().get(BangaruDabbaDao.OF_DATE));
        outgoing.setDealerId(OutgoingListTab.getDealer().getId());
        return outgoing;
    }

    @Override
    public Set<String> getItemNamesStartingWith(String nameLike) {
        return new HashSet<String>();
    }
}