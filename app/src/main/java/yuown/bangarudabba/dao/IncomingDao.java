package yuown.bangarudabba.dao;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Case;
import yuown.bangarudabba.control.IncomingListTab;
import yuown.bangarudabba.model.Incoming;

import static yuown.bangarudabba.control.BangaruDabbaTab.getData;

/**
 * Created by k3n on 18/09/17.
 */

public class IncomingDao extends BangaruDabbaDao<Incoming> {

    private static final IncomingDao INSTANCE = new IncomingDao();

    private IncomingDao() {
        super(Incoming.class);
    }

    public static IncomingDao getInstance() {
        return INSTANCE;
    }

    @Override
    public Incoming transformEntity(Incoming from) {
        Incoming to = new Incoming();
        to.setId(from.getId());
        to.setName(from.getName());
        to.setWeight(from.getWeight());
        to.setPurity(from.getPurity());
        to.setNetPurity(from.getNetPurity());
        to.setDate(from.getDate());
        to.setOfDate(from.getOfDate());
        to.setDealerId(from.getDealerId());
        return to;
    }

    @Override
    public Incoming newEntity(String id) {
        Incoming incoming = new Incoming();
        incoming.setDate(getData().get(BangaruDabbaDao.DATE).toString());
        incoming.setOfDate((Date) getData().get(BangaruDabbaDao.OF_DATE));
        incoming.setDealerId(IncomingListTab.getDealer().getId());
        return incoming;
    }


    @Override
    public Set<String> getItemNamesStartingWith(String nameLike) {
        Set<String> allNamesFromDb = new HashSet<String>();
        realm.beginTransaction();
        List<Incoming> all = realm.where(Incoming.class).like("name", nameLike + '*', Case.INSENSITIVE).findAll();
        realm.commitTransaction();
        for (Incoming dbEntry : all) {
            allNamesFromDb.add(dbEntry.getName());
        }
        return allNamesFromDb;
    }
}