package yuown.bangarudabba.adapter;

import android.content.Context;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import java.util.List;

import yuown.bangarudabba.BangaruDabbaMainActivity;
import yuown.bangarudabba.R;
import yuown.bangarudabba.control.IncomingListTab;
import yuown.bangarudabba.control.OutgoingListTab;
import yuown.bangarudabba.dao.DealerDao;
import yuown.bangarudabba.databinding.DealerRowBinding;
import yuown.bangarudabba.model.Dealer;
import yuown.bangarudabba.model.Frag;
import yuown.bangarudabba.popup.PopupHelper;

/**
 * Created by k3n on 20/09/17.
 */

public class DealerAdapter extends BangaruDabbaAdapter<Dealer, DealerRowBinding, DealerDao> {

    private IncomingListTab incomingListTab;

    private OutgoingListTab outIncomingListTab;

    public DealerAdapter(Context context, List<Dealer> dataList) {
        super(context, dataList, R.layout.dealer_row);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (null == incomingListTab) {
            incomingListTab = new IncomingListTab();
        }
        if (null == outIncomingListTab) {
            outIncomingListTab = new OutgoingListTab();
        }

        DealerRowBinding binding = inflate(viewGroup);
        View rowView = binding.getRoot();
        binding.setDealer(dataList.get(position));
        binding.setAdapter(this);
        binding.dealerOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(view.getContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.dealer_popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new DealerPopupMenuListener(position));
                popup.show();
            }
        });
        binding.addIncomingCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incomingListTab.setDealer(dataList.get(position));
                BangaruDabbaMainActivity.instance.replaceFragment(incomingListTab, Frag.INCOMING, true);
            }
        });
        binding.addOutgoingCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                outIncomingListTab.setDealer(dataList.get(position));
                BangaruDabbaMainActivity.instance.replaceFragment(outIncomingListTab, Frag.OUTGOING, true);
            }
        });
        return rowView;
    }

    @Override
    protected DealerDao getDao() {
        return DealerDao.getInstance();
    }

    @Override
    public void valueChanged(Dealer row) {
//        notifyDataSetChanged(); // Uncommenting this line loses keyboard control on text fields
    }

    private class DealerPopupMenuListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        public DealerPopupMenuListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.dealer_popup_edit:
                    PopupHelper.showDealerEditPopup(BangaruDabbaMainActivity.instance, dataList.get(position));
                    return true;
                case R.id.dealer_popup_remove:
                    getDao().remove(dataList.get(position));
                    BangaruDabbaMainActivity.instance.getDealersTab().reloadEntries();
                    return true;
            }
            return false;
        }
    }
}
