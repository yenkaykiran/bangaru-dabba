package yuown.bangarudabba.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import yuown.bangarudabba.dao.BangaruDabbaDao;
import yuown.bangarudabba.model.BangaruDabbaEntity;

/**
 * Created by kiran.nk on 9/22/2017.
 */

public abstract class BangaruDabbaAdapter<Type extends BangaruDabbaEntity, Binding extends ViewDataBinding, Dao extends BangaruDabbaDao> extends BaseAdapter {

    protected List<Type> dataList;
    protected Context context;
    protected LayoutInflater layoutInflater = null;
    protected int resourceId;

    public BangaruDabbaAdapter(Context context, List<Type> dataList, int resourceId) {
        this.context = context;
        this.dataList = dataList;
        this.resourceId = resourceId;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Type getItem(int i) {
        return dataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void removeEntry(Type entry) {
        dataList.remove(entry);
        if (StringUtils.isNotBlank(entry.getId())) {
            getDao().remove(entry);
        }
        optionalUpdateView();
        notifyDataSetChanged();
    }

    public void optionalUpdateView() {
    }

    protected Binding inflate(ViewGroup viewGroup) {
        return DataBindingUtil.inflate(layoutInflater, resourceId, viewGroup, false);
    }

    protected abstract Dao getDao();

    protected abstract void valueChanged(Type entry);
}
