package yuown.bangarudabba.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Set;

import yuown.bangarudabba.R;
import yuown.bangarudabba.dao.DealerDao;
import yuown.bangarudabba.dao.IncomingDao;
import yuown.bangarudabba.databinding.IncomingRowBinding;
import yuown.bangarudabba.lib.Helper;
import yuown.bangarudabba.model.Dealer;
import yuown.bangarudabba.model.Incoming;

/**
 * Created by k3n on 10/09/17.
 */

public class IncomingAdapter extends BangaruDabbaAdapter<Incoming, IncomingRowBinding, IncomingDao> {

    private IncomingRowBinding binding;

    public IncomingAdapter(Context context, List<Incoming> dataList) {
        super(context, dataList, R.layout.incoming_row);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        List<Dealer> dealers = DealerDao.getInstance().getAll();
        binding = inflate(viewGroup);
        View rowView = binding.getRoot();
        binding.setIncoming(dataList.get(position));
        binding.setAdapter(this);
        if (position % 2 == 0) {
            rowView.setBackgroundResource(R.color.rowColor1);
        } else {
            rowView.setBackgroundResource(R.color.rowColor2);
        }
        return rowView;
    }

    @Override
    protected IncomingDao getDao() {
        return IncomingDao.getInstance();
    }

    @Override
    public void valueChanged(Incoming row) {
        String value = Helper.toThreeDecimalPlaces(Helper.stringToDouble(row.getPurity()) * Helper.stringToDouble(row.getWeight()) / 100);
        row.setNetPurity(value);
        if(null != binding) {
            if(StringUtils.length(row.getName()) >= 1) {
                Set<String> names = getDao().getItemNamesStartingWith(row.getName());
                String[] uniqueNames = (String[]) names.toArray(new String[names.size()]);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, uniqueNames);
                binding.name.setAdapter(adapter);
                binding.notifyChange();
            }
        }
//        notifyDataSetChanged(); // Uncommenting this line loses keyboard control on text fields
    }
}