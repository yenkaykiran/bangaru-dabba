package yuown.bangarudabba.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import yuown.bangarudabba.R;
import yuown.bangarudabba.control.OutgoingListTab;
import yuown.bangarudabba.dao.DealerDao;
import yuown.bangarudabba.dao.OutgoingDao;
import yuown.bangarudabba.databinding.OutgoingRowBinding;
import yuown.bangarudabba.lib.Helper;
import yuown.bangarudabba.model.Dealer;
import yuown.bangarudabba.model.Outgoing;
import yuown.bangarudabba.model.OutgoingType;

/**
 * Created by k3n on 10/09/17.
 */

public class OutgoingAdapter extends BangaruDabbaAdapter<Outgoing, OutgoingRowBinding, OutgoingDao> {

    private OutgoingListTab outgoingListTab;

    public OutgoingAdapter(Context context, List<Outgoing> dataList, OutgoingListTab outgoingListTab) {
        super(context, dataList, R.layout.outgoing_row);
        this.outgoingListTab = outgoingListTab;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        List<Dealer> dealers = DealerDao.getInstance().getAll();
        final OutgoingRowBinding binding = inflate(viewGroup);
        View rowView = binding.getRoot();
        binding.setOutgoing(dataList.get(position));
        binding.setAdapter(this);
        binding.outgoingType.setAdapter(new ArrayAdapter<OutgoingType>(viewGroup.getContext(),
                android.R.layout.simple_list_item_1,
                OutgoingType.values()));
        binding.outgoingType.setSelection(OutgoingType.valueOf(dataList.get(position).getOutgoingType()).ordinal());
        binding.outgoingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dataList.get(position).setOutgoingType(adapterView.getItemAtPosition(i).toString());
                valueChanged(dataList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        if (position % 2 == 0) {
            rowView.setBackgroundResource(R.color.rowColor1);
        } else {
            rowView.setBackgroundResource(R.color.rowColor2);
        }
        return rowView;
    }

    @Override
    protected OutgoingDao getDao() {
        return OutgoingDao.getInstance();
    }

    @Override
    public void valueChanged(Outgoing row) {
        String value = "0";
        if (StringUtils.isEmpty(row.getOutgoingType())) {
            row.setOutgoingType(OutgoingType.METAL.toString());
        }
        if (OutgoingType.valueOf(row.getOutgoingType()).equals(OutgoingType.CASH)) {
            value = Helper.toThreeDecimalPlaces(Helper.stringToDouble(row.getAmount()) / Helper.stringToDouble(row.getPurity()));
        } else if (OutgoingType.valueOf(row.getOutgoingType()).equals(OutgoingType.METAL)) {
            value = Helper.toThreeDecimalPlaces(Helper.stringToDouble(row.getAmount()) * Helper.stringToDouble(row.getPurity()) / 100);
        }
        row.setNetPurity(value);
//        notifyDataSetChanged(); // Uncommenting this line loses keyboard control on text fields
    }

    @Override
    public void optionalUpdateView() {
        super.optionalUpdateView();
        outgoingListTab.optionalChangeView();
    }
}